from __future__ import print_function
from constants import *
import smtplib
import os
from os.path import basename
import json

def main():
    # keys generated through cli way cannot be deleted using console
    student_accounts = json.loads(open("student_names.txt", "r").read())

    for student in student_accounts:
        os.system("aws ec2 delete-key-pair --key-name %s"%(student))

if __name__ == "__main__":
    main()