from __future__ import print_function
import os
import json
import subprocess

debug = 6

def add_accounts_from_file(file_name):
    ''' 
    creates account with students name and email, which is sent to them.
    '''
    accounts = []
    passwords = [] 
    f = open(file_name, "r")
    lines = f.read().split("\n")
    for i in range(len(lines)):
        accounts.append(lines[i][:lines[i].index('@')])
        passwords.append( lines[i][:lines[i].index('@')] + lines[i][-4:])
        print("aws iam create-user --user-name StdEnc_" + lines[i][:lines[i].index('@')])
        if debug&2 > 0:
            os.system("aws iam create-user --user-name StdEnc_" + lines[i][:lines[i].index('@')])
    return accounts, passwords

# last 6 of UIN, + netid
def set_student_role(student_ids, passwords):
    import time
    time.sleep(15)
    for id in range(len(student_ids)):
        # creates account with students name and email, which is sent to them.
        print("aws iam create-login-profile --user-name StdEnc_" + student_ids[id] + " --password " + passwords[id]+ " --password-reset-required")
        print("aws iam add-user-to-group --group-name StudentEnclaves --user-name StdEnc_"+ student_ids[id])
        print("aws iam tag-user --user-name StdEnc_" + student_ids[id] + " --tags " + student_ids[id])
        if debug&4 > 0:
            pass
            os.system("aws iam create-login-profile --user-name StdEnc_"+student_ids[id]+" --password " + passwords[id] + " --password-reset-required")
            os.system("aws iam add-user-to-group --group-name StudentEnclaves --user-name StdEnc_"+student_ids[id])
            os.system("aws iam tag-user --user-name StdEnc_" + student_ids[id] + " --tags Key=User,Value=" + student_ids[id])

def create_stacks(student_ids):
    region = "--region us-east-2"
    # ensure body is not out of date.
    template_body = "https://s3.us-east-2.amazonaws.com/cf-templates-1rk7sn0khycpv-us-east-2/20190126P8-TexasCyberRange465EnclaveTopology8rmsi6dtoux"
    
    
    for student_name in student_ids:
        print("aws ec2 create-key-pair --key-name %s > %s.pem"%(student_name, student_name))
        os.system("aws ec2 create-key-pair --key-name %s > %s.pem"%(student_name, student_name))
        parameters = "--parameters ParameterKey=KeyName,ParameterValue=%s ParameterKey=InstanceType,ParameterValue=t2.medium ParameterKey=NetID,ParameterValue=%s"%(student_name, student_name)

        print("aws cloudformation create-stack --stack-name %s --template-url %s %s %s"%(student_name, template_body, parameters, region))
        os.system("aws cloudformation create-stack --stack-name %s --template-url %s %s %s"%(student_name, template_body, parameters, region))
    

def main():
    accounts, passwords = add_accounts_from_file("sampleEmails.txt")
    
    set_student_role(accounts, passwords)

    create_stacks(accounts)

    open("student_names.txt", "w").write(json.dumps(accounts))
    # Moves account # from root to student.
    #aws organizations move-account --account-id <account ID> --source-parent-id r-dvrq --destination-parent-id ou-dvrq-wyk0leit

if __name__ == "__main__":
    main()
